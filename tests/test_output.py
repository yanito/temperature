import unittest
    
    
class OutputTestCase(unittest.TestCase):

    def test_output(self):
        input = open(f"input_files/file1.csv", "rt")
        output = open(f"output_files/example_generated_output_file1.csv", "rt")
        correct_output = open(f"tests/correct_outputs/file1_output.csv", "rt")

        input_list = list(input)
        output_list = list(output)
        correct_output_list = list(correct_output)

        for line in input_list:
            index_of_input_line = input_list.index(line)
            self.assertEqual(str(output_list[index_of_input_line].strip("\n")), str(correct_output_list[index_of_input_line].strip("\n")), True)
        input.close()        
        output.close()
        correct_output.close()
        

if __name__ == '__main__':  
    unittest.main()
